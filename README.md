# Portfolio
Demo web site here: https://nhvu95.com/

Responsive UI is available.

If you use my template, please give me a star, I very much appreciate.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.4.

## Build
1. Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
2. Run `npm run scully`to prerender .md file to blog page.

## Development server
Run `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
Run `npm run scully:serve` to serve static server. Make sure that you run build (1-2) first.

## Page preview
1. Home
![portfolio1](https://user-images.githubusercontent.com/26276890/132381111-231045e3-cc7d-44d9-b886-ee28a8a18da8.png)
2. Blog summary
![portfolio2](https://user-images.githubusercontent.com/26276890/132381150-fd8ea34a-0fc2-4be4-921d-8b06d20a0487.png)

## Code scaffolding
Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
